Module module_koneksi
    Private readDT As MySqlDataReader
    Private GrabIDautoNumber As Integer
#Region "Atur Koneksi"
    Private Stringconnection As String = "server=127.0.0.1;username=MYUSER;password=MYPWD;database=MYDB;"
#End Region
#Region "CRUD"
    '--------------------------------------------------------CRUD--------------------------------------------------
    '//Pencarian data
    Public Function Simpan_Data(ByVal Query As String)
        Dim parameter As Boolean = False
        Try
            Using conn As New MySqlConnection(Stringconnection)
                Using cmnd As New MySqlCommand(Query, conn)
                    conn.Open()
                    cmnd.ExecuteNonQuery()
                    parameter = True
                    conn.Close()
                End Using
            End Using
        Catch ex As Exception
            If ex.Message.Contains("Duplicate entry") Then
                MessageBox.Show(homepage, "Gagal menambahkan" & vbNewLine & "Data Sudah Ada!", "[Duplicate Data]", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                MessageBox.Show(homepage, ex.Message, "Terjadi Kesalahan!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End Try
        Return parameter
    End Function
    Public Function Update_Data(ByVal Query As String, ByVal ParameterDelete As String)
        Dim parameter As Boolean = False
        Try
            Using conn As New MySqlConnection(Stringconnection)
                Using cmnd As New MySqlCommand(Query, conn)
                    conn.Open()
                    readDT = cmnd.ExecuteScalar()
                    parameter = True
                    'If readDT.HasRows Then
                    '    parameter = True
                    'Else
                    '    parameter = False
                    '    MessageBox.Show(homepage, "[GAGAL!!]", "Data Tidak Ditemukan!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    'End If
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(homepage, ex.Message, "Terjadi Kesalahan!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Return parameter
    End Function
    Public Sub Cari_Data(ByVal textSearch As String, ByVal datagrid As DataGridView, ByVal index As Integer)

        Try
            Dim getRow As DataGridViewRow = (
            From row As DataGridViewRow In datagrid.Rows
            Where LCase(row.Cells(index).FormattedValue).ToString.Contains(LCase(textSearch)) Select row
            ).First

            datagrid.Rows(getRow.Index).Selected = True
        Catch ex As Exception
            MsgBox(IIf(ex.Message.Contains("Sequence"), "Data tidak ditemukan", ex.Message), MsgBoxStyle.Exclamation, "")
        End Try
    End Sub
    Public Function View(ByVal tablename) As DataTable
        Try
            Using conn As New MySqlConnection(Stringconnection)
                Using cmnd As New MySqlCommand("SELECT * FROM " & tablename & "", conn)
                    Using DT As New DataTable
                        conn.Open()
                        DT.Load(cmnd.ExecuteReader)
                        conn.Close()
                        Return DT
                    End Using
                End Using
            End Using
        Catch ex As Exception
            MessageBox.Show(homepage, ex.Message, "Terjadi Kesalahan!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End Try
        Return Nothing
    End Function
    'Bagian Ini Berfungsi Untuk Menampilkan data berdasar RowIndexTable
    Public Function SelectData(ByVal tableDB As String, ByVal indexSelect As Integer, ByVal columnTable As String)
        Dim row As DataRow = module_koneksi.View(tableDB).Rows(indexSelect)
        Dim getValue As String = row(columnTable)
        Return getValue
    End Function
    '//Atur nomor kupon
    Public Function GenerateKupon(ByVal Tablename As String, NameColumn As String, ByVal InisialCode As String) As String

        Dim Text As String = String.Empty

        If InisialCode.ToString.Length <> 1 Then
            MessageBox.Show("Panjang karakter melebihi fungsi yang telah ditentukan" & vbNewLine &
                           "Maximal panjang Inisial Code = 1", "Statement ditolak!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Try
                Using conn As New MySqlConnection(Stringconnection)
                    Using Cmnd As New MySqlCommand("SELECT * FROM " & Tablename & " WHERE " & NameColumn & " in (SELECT MAX(" & NameColumn & ") FROM " & Tablename & ")", conn)
                        conn.Open()

                        readDT = Cmnd.ExecuteReader()
                        readDT.Read()
                        If readDT.HasRows Then
                            GrabIDautoNumber = Microsoft.VisualBasic.Mid(readDT.GetString(NameColumn), 2) ' Ambil Value
                            GrabIDautoNumber += 1 ' Hitung 1
                            Text = "ID-" & InisialCode & "" & GrabIDautoNumber.ToString("D5") '000000
                        Else
                            Text = "ID-" & InisialCode & "00001"
                        End If
                        readDT.Close() 'tutup read data
                        conn.Close() 'close koneksi
                    End Using

                End Using
            Catch ex As Exception
                MessageBox.Show(homepage, ex.Message, "Terjadi Kesalahan!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If
        Return Text
    End Function
    'Public Function Tambah_Menu(kodemakanan As Char,
    '                       noID As String,
    '                       makananMinuman As String,
    '                       hargaBazar As Integer,
    '                       hargaAsli As Integer) As Boolean 'Simpan Data Menu
    '    Dim Status As Boolean = False
    '    Try
    '        Using conn As New MySqlConnection(Stringconnection)
    '            conn.Open()


    '        End Using
    '    Catch ex As Exception

    '    End Try

    '    Return Status
    'End Function
#End Region

End Module
