Module Bazarfunction
    Public Sub ClearAllControls(ByVal yourCtrlOBJ As Control)
        For Each clearT In yourCtrlOBJ.Controls
            If clearT.Tag = "del" Then
                clearT.Text = String.Empty
            End If
        Next
    End Sub
    'Atur Datagrid
    Public Sub PropertiesDatagird(ByVal Grid As DataGridView)
        With Grid
            .AllowUserToAddRows = False
            .AllowUserToDeleteRows = False
            .MultiSelect = False
            .ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize
            .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
            .SelectionMode = DataGridViewSelectionMode.FullRowSelect
            .ReadOnly = True


            '----hitung row---
            'For row = 0 To Grid.RowCount - 1
            '    Grid.Rows(row).Cells(0).Value = row + 1
            'Next
        End With
    End Sub
    '//PlaceHolder text
    Private Declare Function GetWindow Lib "user32.dll" (ByVal hwnd As Integer, ByVal wCmd As Integer) As Integer
    Private Declare Auto Function SendMessageString Lib "user32.dll" Alias "SendMessageA" (ByVal hwnd As Integer, ByVal wMsg As Integer, ByVal wParam As Integer, ByVal lParam As String) As Integer
    Const GW_CHILD = 9
    Const EM_SETCUEBANNER = &H1501
    '//Menampilkan Pesan Yes dan No

    Public Function MSG_QUESTION(ByVal Text As String, ByVal title As String) As Boolean
        If MessageBox.Show(homepage, Text, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Return True
        Else
            Return False
        End If
    End Function
    Sub SetWatermark(ByVal Ctl As TextBox, ByVal Txt As String)
        SendMessageString(Ctl.Handle, EM_SETCUEBANNER, 1, Txt)
    End Sub
    '//Membuat event agar hanya menginput TipeData Integer (Nomor) | Block String
    Sub NumbericOnly(ByVal e As KeyPressEventArgs)
        If Not IsNumeric(e.KeyChar) And Not e.KeyChar = ChrW(Keys.Back) Then
            e.Handled = True
        End If
    End Sub
    '//Show Child Form (MDI FORM)
    Public Sub closeChildForm()
        For Each ChildForm As Form In homepage.MdiChildren
            ChildForm.Close()
        Next
    End Sub
    Public Sub anyfrm(ByVal frm As Object)
        If homepage.Home_ToolStripButton.Selected = True Then
            homepage.PictureBox1.Visible = True
        Else
            homepage.PictureBox1.Visible = False
            homepage.IsMdiContainer = True
            With frm
                .MdiParent = homepage
                .Dock = DockStyle.Left
                .Show()
            End With
        End If

    End Sub
    Public Function GenerateCodeBazar()
        'tanggal-bulan-tahun-jam-menit-detik

        '//tanggal
        With Now
            Return .Day & .Month & .Year & .Hour & Now.Minute & Now.Second
        End With
    End Function
    'Fungsi untuk memperbolehkan hanya Nomor
    'Event yang digunakan (Keypress)
    Public Sub TextNumberic(KeyPress As KeyPressEventArgs)
        If Not IsNumeric(KeyPress.KeyChar) And Not KeyPress.KeyChar = ChrW(Keys.Back) Then
            KeyPress.Handled = True
        End If
    End Sub
    Public Function BazarValidasiHarga(ByVal hargaBazar As Integer, ByVal hargaAsli As Integer)
        'Pengecekan Apakah harga bazar kurang dari harga asli
        If hargaBazar <= hargaAsli Then
            Return False
        Else
            Return True
        End If
    End Function


End Module
